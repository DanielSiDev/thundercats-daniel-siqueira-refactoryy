# M4U Espresso

Tenha certeza que você leu **todo** este documento com cuidado, e siga as instruções
contidas nele. Preste bastente atenção na sessão "O que a gente acredita". Nós também
recomendamos a seguinte leitura:
[9 Essential Tips on How to Tackle a Coding Challenge](https://www.codementor.io/learn-programming/9-essential-tips-tackle-coding-challenge).

## Background

Nós gostamos muito de café.

Então criamos um app para servir café para as pessoas de nosso barista favorito.

O app mantem o controle dos pedidos de café; o balancete de consumo de cada pessoa; o que cada pessoa já pagou; e o que ainda estão devendo.

## dados

A gente tem os seguintes dados.

- `data/prices.json` - passados pelo nosso barista. Contém os detalhes das bebidas disponíveis e seus respectivos preços.
- `data/orders.json` - lista das bebidas que foram pedidas pelos clientes no App.
- `data/payments.json` - lista dos pagamentos feitos por cada cliente dos itens consumidos por eles.

## Requisitos

- Carregar a lista de preços
- Carregar os pedidos
  - Calcular o total dos pedidos dos clientes
- Carregar os dados de pagamentos
  - Calcular o total pago por cada cliente
  - Calcular o saldo devedor de cada cliente
- Retornar um JSON contendo o resultado desse trabalho.

(veja `spec/coffee_app_integration_spec.rb` para alguns exemplos específicos)

## Começando o jogo

Criei um diretório de trabalho para seu projeto. Copie `m4u_thundercats_estudo.tar.gz` dentro do diretório criado. Para extrair os arquivos execute:
```
tar -xzvpf m4u_thundercats_estudo.tar.gz
```

M4U Espresso requer que ruby e bundler estejam instalados. Antes de começar; instale as dependências executando o comando `bundle`.
Uma vez as dependências esteja, instaladas você terá disponível alguns comandos:

- `bundle exec rake`  : Rodará sua aplicação dando o resultado no terminal.
- `bundle exec rspec` : Rodará a suite de testes, adicionamos alguns testes de integração para você pegar o espírito da coisa mas provavelmente você precisará de muito mais durante o seu aprendizado.

## O que a gente acredita

- *Clean*, *readable*, *production quality code*; Podemos usar seu código em nosso ambiente produtivos?
- Uma boa modelagem OO e boas decisões de design. Estamos esperando uma solução *object-oriented* , mesmo que você pense que escrevendo o código de forma procedural faça mais sentido para esse problema.
- Extensible code; adicionar novas funcionalidades será um exercício que faremos quando você terminar essa primeira parte.
- Bom uso da linguagem Ruby e sua convenção.
- *Solid testing approach* _(dica - você deve escrever muito mais testes dos que já existem)_
- Use Git (passarei um repo para isso)!
 - Commit alterações pequenas para que possamos acompanhar o seu desenvolvimento e progresso.

Não existem pegadinha no código. Não exagere no pensamento. Apenas escreve um bom e sólido código.