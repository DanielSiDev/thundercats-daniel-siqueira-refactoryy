module CoffeeApp
  class CoffeeFactory
    def initialize(prices)
      @prices_table = JSON.parse(prices, symbolize_names: true)
      @coffees = {}
    end

    def build
      @prices_table.each do |item|
        coffee = Coffee.new(name: item[:drink_name])

        item[:prices].each do |price|
          coffee.add_size(CoffeeVariant.new(size: price[0].to_s, price: price[1]))
        end

        @coffees[coffee.name.to_sym] = coffee
      end

      @coffees
    end
  end
end