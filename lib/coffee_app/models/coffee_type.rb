# frozen_string_literal: true

require File.expand_path(File.join(File.dirname(__FILE__), './size'))
module CoffeeApp
  # Model for Coffee App
  class CoffeeType
    attr_reader :name, :size
    def initialize(name)
      @name = name
      @prices = []
    end

    def add_size(size_name)
      @size = CoffeeApp::Size.new(size_name)
    end

    def add_prices(size_name, price)
      @size_coffee = CoffeeApp::Size.new(size_name)
      @size_coffee.add_price(price)
      prices << { size: @size_coffee.name, price: @size_coffee.price.value }
    end
  end
end
