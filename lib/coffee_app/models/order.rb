# frozen_string_literal: true

require File.expand_path(File.join(File.dirname(__FILE__), './coffee'))
module CoffeeApp
  # Model for Order
  class Order
    attr_reader :code, :order_owner, :coffee, :coffees
    def initialize(owner_name)
      @order_owner = owner_name
      @coffees = []
    end

    # Generate a alphanumeric code(hash) with 100 characteres different each call
    def generate_code
      @code = rand(36**100).to_s(36)
    end

    def add_coffee(coffee )
      @coffees << coffee
      @coffee = coffee
    end
  end
end
