# frozen_string_literal: true

require File.dirname(__FILE__) + './order.rb'
require File.dirname(__FILE__) + './payments.rb'

module CoffeeApp
  # Model for Account
  class Account
    include Pay
    attr_reader :order, :payment
    def initialize; end

    # Generate a alphanumeric code(hash) with 100 characteres different each call
    def generate_code
      @code = rand(36**100).to_s(36)
    end

    def add_order
      @order = CoffeeApp::Order.new
    end

    def add_payment(amount)
      @payment = CoffeeApp::Payment.new(amount)
    end
  end
end
