# frozen_string_literal: true

module CoffeeApp
  # Model for Payments
  class Payments
    attr_reader :amount, :code
    def initialize(amount)
      @amount = amount
      generate_code
    end

    # Generate a alphanumeric code with 8 characteres different each call
    def generate_code
      @code = rand(36**8).to_s(36)
    end
  end
end
