# frozen_string_literal: true

require File.expand_path(File.join(File.dirname(__FILE__), './price'))
module CoffeeApp
  # Model for Size
  class Size
    attr_reader :name, :price

    def initialize(name)
      @name = name
    end

    def add_price(price)
      @price = Price.new(price)
    end
  end
end
