# frozen_string_literal: true

require 'json'
module CoffeeApp
  # Model for load json
  class FileLoadJson
    def initialize(path_name)
      @path = File.expand_path(path_name)
    end

    def load_file
      file = File.read(@path)
      JSON.parse(file) # symbolize_names:true
    end
  end
end
