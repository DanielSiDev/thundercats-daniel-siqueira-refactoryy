# frozen_string_literal: true

require File.expand_path(File.join(File.dirname(__FILE__), './coffee_type'))
module CoffeeApp
  # Model for Coffee
  class Coffee
    attr_reader :name, :sizes, :coffee_type

    def initialize(name)
      @name = name
      @sizes = []
    end

    def add_coffee_type_size(size)
      @coffee_type = CoffeeApp::CoffeeType.new(@name)
      @coffee_type.add_size(size)
      @coffee_type
    end

    def add_coffee_type_price(size_name, price_value)
      @coffee_type = CoffeeApp::CoffeeType.new(@name)
      @coffee_type.add_prices(size_name, price_value)
      @coffee_type
    end

    def add_size(coffee_variant)
      @sizes << coffee_variant
      @sizes
    end
  end
end
