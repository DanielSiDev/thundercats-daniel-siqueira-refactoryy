# frozen_string_literal: true

require File.dirname(__FILE__) + './account'
require File.dirname(__FILE__) + './order'

module CoffeeApp
  # Model for Client
  class Client
    attr_reader :name, :account, :order

    def initialize(name)
      @name = name
    end

    def generate_account
      @account = CoffeeApp::Account.new
    end

    def generate_order
      @order = CoffeeApp::Order.new
    end
  end
end
