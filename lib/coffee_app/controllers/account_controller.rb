# frozen_string_literal: true

require File.expand_path(File.join(File.dirname(__FILE__), '../models/coffee'))
require File.expand_path(File.join(File.dirname(__FILE__), '../models/size'))
require File.expand_path(File.join(File.dirname(__FILE__), '../models/price'))
require File.expand_path(File.join(File.dirname(__FILE__), './price_controller'))
require File.expand_path(File.join(File.dirname(__FILE__), './order_controller.rb'))
require File.expand_path(File.join(File.dirname(__FILE__), './payments_controller.rb'))
require 'byebug'
module CoffeeApp
  # Controller for Account
  class AccountController

    def initialize()
        @order = OrderController.new     
        @payment = PaymentController.new
    end

    def payments_by_client(client)
    end

    def orders_by_client(client)
    end

    def build_balance(consume_key, consume_value, balance_user, paid)
        key = consume_key.to_s
        balance_user[key] = if consume_value > paid[key]
                              consume_value - paid[key]
                            else
                              paid[key] - consume_value
                            end
        balance_user
      end
    
      def balance(order_json, prices_json, payments_json)
        balance_user = {}
        consume = @order.consume_total(order_json, prices_json)
        paid = @payment.payments_total(payments_json)
        consume.each do |c_key, c_value|
          build_balance(c_key, c_value, balance_user, paid)
        end
        balance = balance_user
        balance
      end


  end
end

o = CoffeeApp::OrderController.new
orders_json = o.orders_json
pr = CoffeeApp::PriceController.new
prices_json = pr.prices_json
pa = CoffeeApp::PaymentController.new
payments_json = pa.payments_json

ac = CoffeeApp::AccountController.new
balance = ac.balance(orders_json, prices_json, payments_json)

puts balance