# frozen_string_literal: true

require File.expand_path(File.join(File.dirname(__FILE__), '../models/file_load_json'))
require File.expand_path(File.join(File.dirname(__FILE__), '../models/coffee'))
require File.expand_path(File.join(File.dirname(__FILE__), '../models/order'))
require File.expand_path(File.join(File.dirname(__FILE__), '../controllers/price_controller'))
require 'byebug'
module CoffeeApp
  # Controller for Order
  class OrderController
    def initialize
      @file_orders = CoffeeApp::FileLoadJson.new './data/orders.json'
      @orders = @file_orders.load_file      
    end

    def orders_json
      @orders.to_json
    end



    def order_list(orders_json)
      orders = []
      list_order = JSON.parse(orders_json, symbolize_names: true)
      list_order.each do | order |
        coffee = CoffeeApp::Coffee.new(order[:drink])
        coffee.add_coffee_type_size(order[:size])
        order = CoffeeApp::Order.new(order[:user])
        order.add_coffee(coffee)
        orders.push(order)
      end
      orders
    end

    def total_order_user
      total_order_client = {}
      order_list(orders_json).each do |p|
        total_order_client[p.order_owner] = if total_order_client.key? p.order_owner
                                            total_order_client[p.order_owner] + 1
                                          else
                                            1
                                           end
      end
      total_order_client.to_json
    end

    def build_total_consume(prices_json, orders, consume)
      order = orders.order_owner
      coffee_name = orders.coffee.name
      coffee_size = orders.coffee.coffee_type.size.name
      if consume.key? order
        line_price = @price.price_by_drink_name_size(prices_json, coffee_name, coffee_size)
        consume[order] = consume[order] + line_price
      else
        consume[order] = @price.price_by_drink_name_size(prices_json, coffee_name, coffee_size)
      end
      consume
    end

    def consume_total(order_json, prices_json)
      @price = CoffeeApp::PriceController.new
      consume = {}
      order_list(order_json).each do |orders|
        build_total_consume(prices_json, orders, consume)
      end
      consume
    end
  end
end

#  o = CoffeeApp::OrderController.new
# orders_json = o.orders_json
# p = CoffeeApp::PriceController.new
# prices_json = p.prices_json

# consume = o.consume_total(orders_json, prices_json)
# puts consume

# ord = o.total_order_user
# puts ord

# puts ord[0].order_owner
# puts ord[0].coffee.name
# puts ord[0].coffee.coffee_type.size.name
