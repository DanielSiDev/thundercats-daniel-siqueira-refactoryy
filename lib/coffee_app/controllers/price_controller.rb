# frozen_string_literal: true

require File.expand_path(File.join(File.dirname(__FILE__), '../models/file_load_json'))
require File.expand_path(File.join(File.dirname(__FILE__), '../models/coffee'))
require File.expand_path(File.join(File.dirname(__FILE__), '../models/price'))
require File.expand_path(File.join(File.dirname(__FILE__), '../models/size'))
require 'byebug'
module CoffeeApp
  # Controller for Price
  class PriceController
    def initialize
      @file_path = CoffeeApp::FileLoadJson.new 'data/prices.json'
      @prices = @file_path.load_file
    end

    def prices_json
      @prices.to_json
    end

    def price_by_drink_name_size(prices_json, drink_name, size_name)
      @drink_name = drink_name.to_s
      @size_name = size_name.to_s
      prices = JSON(prices_json, symbolize_names: true)
      prices.each do | price |
        if price[:drink_name] == @drink_name
          price[:prices].each do |k, v|
            if k.to_s == @size_name
              return v 
            end
          end
        end
      end
    end
  end
end

# s = CoffeeApp::PriceController.new
# prices = s.prices_json
# price = s.price_by_drink_name_size(prices, 'mocha', 'large')
#    puts price