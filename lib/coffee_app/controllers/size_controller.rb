# frozen_string_literal: true

require File.expand_path(File.join(File.dirname(__FILE__), '../models/coffee'))
require File.expand_path(File.join(File.dirname(__FILE__), '../models/size'))
require File.expand_path(File.join(File.dirname(__FILE__), '../models/price'))
require File.expand_path(File.join(File.dirname(__FILE__), './price_controller'))
require 'byebug'
module CoffeeApp
  # Controller for Size
  class SizeController
    attr_reader :prices_json
    def initialize
      @prices_json = CoffeeApp::PriceController.new.prices_json
    end

    def size_list(prices_json)
      size_list = []
      prices = JSON(prices_json, symbolize_names: true)
      prices.each do |p|
        coffee = CoffeeApp::Coffee.new(p[:drink_name])
        p[:prices].each do |k, v|
          size = CoffeeApp::Size.new(k)
          size.add_price(v)
          size_list << size
        end
      end
      size_list
    end
  end
end

s = CoffeeApp::SizeController.new
prices = s.prices_json
sizes =  s.size_list(prices)
price = s.price_by_drink_name(prices, 'mocha', 'large')
   puts price
# puts sizes[0].name
# puts sizes[0].price.value

# price.each do |p|
#   puts p.name
# end
