# frozen_string_literal: true

require File.expand_path(File.join(File.dirname(__FILE__), '../models/file_load_json'))

require 'json'
module CoffeeApp
    # class
    class PaymentController
    def initialize
        @file_path = CoffeeApp::FileLoadJson.new 'data/payments.json'
        @payments = @file_path.load_file
        # @order = OrderController.new
        # @price = PriceController.new
    end

    def payments_json
        @payments.to_json
    end

    def payments_total(payments_json)
        paid_by_client = {}
        payments = JSON.parse(payments_json, symbolize_names: true)
        payments.each do |p|
            user = p[:user]
            u_cli = paid_by_client[user]
            amount = p[:amount]
            paid_by_client[user] = (paid_by_client.key? user) ? u_cli + amount : amount
        end
        paid_by_client
    end

    end
end

# s = CoffeeApp::PaymentController.new
# pay = s.payments_json
# payments_total = s.payments_total(pay)
#    puts payments_total