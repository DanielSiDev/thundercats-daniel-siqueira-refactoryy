# # frozen_string_literal: true

# require 'coffee_app/factories/coffee_factory'
# require 'coffee_app/models/coffee'
# require 'coffee_app/models/coffee_variant'
# require 'coffee_app/models/file_load_json'
# module CoffeeApp
#   def self.call(prices_table_json, _orders_json, _payments_json)
#     coffees = CoffeeApp::CoffeeFactory.new(prices_table_json).build
#   end
# end

# ## precisamos mockar para passar no teste
# # temos Coffee que tem Size
# frozen_string_literal: true

# Comece por aqui .....
require File.expand_path(File.join(File.dirname(__FILE__), 'coffee_app/controllers/order_controller'))
require File.expand_path(File.join(File.dirname(__FILE__), 'coffee_app/controllers/payments_controller'))
require File.expand_path(File.join(File.dirname(__FILE__), 'coffee_app/controllers/price_controller'))
require File.expand_path(File.join(File.dirname(__FILE__), 'coffee_app/controllers/account_controller'))
module CoffeeApp
    # Main Class
    class CoffeeMain
    attr_reader :user_order_consume, :user_payments_pay, :user_payments_balance, :user_orders
    def initialize
        @order = OrderController.new
        @payment = PaymentController.new
        @price = PriceController.new
        @account = AccountController.new
    end

    def build_coffee(prices_json, orders_json, payments_json)
        # Total consumido pelos Clientes
        @user_order_consume = @order.consume_total(orders_json, prices_json)
        # Total pago pelos Clientes
        @user_payments_pay = @payment.payments_total(payments_json)
        # Saldo Devedor
        @user_balance = @account.balance(orders_json, prices_json, payments_json)
        # Pedidos de cada Cliente
        @user_orders = JSON.parse(@order.total_order_user)
    end

        def call(prices_json, orders_json, payments_json)
            build_coffee(prices_json, orders_json, payments_json)
            data_list = []
            @user_orders.each do |key, _value|
                data = { "user": key, "order_total": @user_order_consume[key.to_s], "payment_total": @user_payments_pay[key.to_s], "balance": @user_balance[key.to_s] }
                data_list.push(data)
            end
            data_list.to_json
        end

    end
end
