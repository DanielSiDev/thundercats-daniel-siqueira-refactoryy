require 'spec_helper'

RSpec.describe CoffeeApp::CoffeeFactory do

  subject { CoffeeApp::CoffeeFactory.new(prices_json).build }

  context 'Build a simple json' do
    let(:prices_json) do
      <<-JSON
      [
        {
          "drink_name": "short espresso",
          "prices": {
            "small": 3.0
          }
        }
      ]
      JSON
    end

    it('A list with one coffee object') { expect(subject.size).to be 1 }
    it('The built coffee') do
      expect(subject[:'short espresso'].name).to eq('short espresso')
      expect(subject[:'short espresso'].class).to eq(CoffeeApp::Coffee)
      expect(subject[:'short espresso'].sizes.size).to be 1
    end
  end

end
