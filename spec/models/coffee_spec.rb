require 'spec_helper'

RSpec.describe CoffeeApp::Coffee do

  describe 'add_size' do
    context 'Add a coffee variant' do
      subject { CoffeeApp::Coffee.new(name: 'carioca').add_size(CoffeeApp::CoffeeVariant.new(size: 'pequeno', price: 3.0)) }

      it 'has a size on it' do
        expect(subject.sizes.size).to be 1
      end
    end
  end
end
